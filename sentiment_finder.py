import sys
from textblob import TextBlob
import numpy as np

# pass the text file via command prompt to get the final polarity


c=[]
f = open(str(sys.argv[1]),"r")
y = f.read()

blob = TextBlob(str(y))

for sentence in blob.sentences:
	c.append(sentence.sentiment.polarity)
print(np.mean(c))


#python sentiment_finder.py E:\python_codes\sentiment_finder_sample_input.txt